// Copyright Epic Games, Inc. All Rights Reserved.

#include "FirstGameGameMode.h"
#include "FirstGameCharacter.h"
#include "GameFramework/Actor.h"
#include "Kismet/gameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

AFirstGameGameMode::AFirstGameGameMode()
{
	PrimaryActorTick.bCanEverTick = true;


}

void AFirstGameGameMode::BeginPlay() {
	Super::BeginPlay();

	if (spawnTimes.IsZero())
		spawnTimes.Set(2, 5);

	FTimerHandle mytimeHandler;
	GetWorldTimerManager().SetTimer(
		mytimeHandler, this, &AFirstGameGameMode::SpawnRech, 
		FMath::RandRange(spawnTimes.X, spawnTimes.Y), true
	);

	timeHandler = mytimeHandler;
	UE_LOG(LogTemp, Warning, TEXT("Start"));

}

void AFirstGameGameMode::Tick(float deltaTime){
	Super::Tick(deltaTime);
}

void AFirstGameGameMode::SpawnRech(){
	UE_LOG(LogTemp, Warning, TEXT("SPAWN"));
	float randX = FMath::RandRange(spawnMin.X,spawnMax.X);
	float randY = FMath::RandRange(spawnMin.Y,spawnMax.Y);

	FVector spawnPos = FVector(randX, randY, spawnHeight);
	FRotator rot = FRotator::ZeroRotator;

	GetWorld()->SpawnActor(playerRech, &spawnPos, &rot);
}

void AFirstGameGameMode::PlayerDeath(){
	UE_LOG(LogTemp, Warning, TEXT("Timer stopped"));

	GetWorldTimerManager().ClearTimer(timeHandler);
}

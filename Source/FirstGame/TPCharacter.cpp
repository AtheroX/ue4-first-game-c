// Fill out your copyright notice in the Description page of Project Settings.


#include "TPCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/gameplayStatics.h"
#include "FirstGameGameMode.h"


// Sets default values
ATPCharacter::ATPCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Resize
	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

	//Que no tenga rotaciones de ning�n tipo
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	//Rota autom�ticamente el character hacia su movimiento 
	GetCharacterMovement()->bOrientRotationToMovement = true;
	//Vel de rotaci�n
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	//Cantidad de salto y velocidad de movimiento en el aire (% a la total)
	GetCharacterMovement()->JumpZVelocity = 600.0f;
	GetCharacterMovement()->AirControl = 0.2f;

	//Crea c�mara y la pone hija de root
	cameraMovement = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera"));
	cameraMovement->SetupAttachment(RootComponent);
	// Distancia a jugador y camara rota al rededor de su punto final
	cameraMovement->TargetArmLength = 300.0f;
	//Si no quitamos esto hace que no gire autom�ticamente hacia donde se mueve
	cameraMovement->bUsePawnControlRotation = false;

	// Crea followcamera y la pone hija de el final de la c�mara
	followCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	followCamera->SetupAttachment(cameraMovement, USpringArmComponent::SocketName);
	followCamera->bUsePawnControlRotation = false;


	dead = false;
	power = 100.0f;
}


// Called when the game starts or when spawned
void ATPCharacter::BeginPlay()
{
	Super::BeginPlay();
	//Al empezar el juego anclamos la funci�n de qu� hacer al colisionar
	//Por as� decirlo declaramos nuestro onCollisionEnter
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ATPCharacter::OnBeginOverlap);


	//Comprobar que se ha puesto en el Blueprint
	if (powerWidgetClass != nullptr) {
		powerWidget = CreateWidget(GetWorld(), powerWidgetClass);
		powerWidget->AddToViewport();
	}



}

// Called every frame
void ATPCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!dead) {
		power = power - DeltaTime * powerTreshold;

		if (power <= 0) {
			power = 0;
			dead = true;

			GetMesh()->SetSimulatePhysics(true);

			FTimerHandle timeHandler;
			GetWorldTimerManager().SetTimer(
				timeHandler, this, &ATPCharacter::RestartGame, 3.0, false
			);	
			GetWorld()->GetAuthGameMode<AFirstGameGameMode>()->PlayerDeath();

		}
	}

}


// Called to bind functionality to input
void ATPCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Linkea el AxisMapping de Turn al yaw (rotaci�n horizontal - Z)
	PlayerInputComponent->BindAxis("Turn", this, &ATPCharacter::ControlYawInput);
	// LookUp usa la rotaci�n vertical (X)
	PlayerInputComponent->BindAxis("LookUp", this, &ATPCharacter::ControlPitchInput);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ATPCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ATPCharacter::UnJump);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATPCharacter::MoveRight);

}

void ATPCharacter::ControlYawInput(float val) {
	if (dead) return;
	if (val != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		PC->AddYawInput(val);
	}
}

void ATPCharacter::ControlPitchInput(float val){
	if (dead) return;
	if (val != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		PC->AddPitchInput(val);
	}

}


void ATPCharacter::MoveForward(float vel) {

	if (dead)
		return;

	//Cojemos toda la Rotation de nuestra transform
	const FRotator rot = Controller->GetControlRotation();

	//�nicamente nos quedamos con el y (horizontal)
	const FRotator yawRot = FRotator(0, rot.Yaw, 0);

	//Saca el Forward de nuestro character
	const FVector dir = FRotationMatrix(yawRot).GetUnitAxis(EAxis::X);


	AddMovementInput(dir, vel);

}

void ATPCharacter::MoveRight(float vel) {

	if (dead)
		return;

	//Cojemos toda la Rotation de nuestra transform
	const FRotator rot = Controller->GetControlRotation();

	//�nicamente nos quedamos con el y (horizontal)
	const FRotator yawRot = FRotator(0, rot.Yaw, 0);

	//Saca el Right de nuestro character
	const FVector dir = FRotationMatrix(yawRot).GetUnitAxis(EAxis::Y);


	AddMovementInput(dir, vel);

}

void ATPCharacter::Jump(){
	if (dead)
		return;
	bPressedJump = true;
	JumpKeyHoldTime = 0.0f;
}

void ATPCharacter::UnJump(){
	bPressedJump = false;
	ResetJumpState();
}


void ATPCharacter::OnBeginOverlap(UPrimitiveComponent* hit, AActor* other,
	UPrimitiveComponent* otherComp, int32 otherIndex, 
	bool bFromSweep, const FHitResult& hitRes) {

	if (other->ActorHasTag("Pon") && !dead) {
		UE_LOG(LogTemp, Warning, TEXT("Collided"));
		power = power + 10.0f > 100.0f ? 100.0f : power + 10.0f;
		other->Destroy();
	}
}

void ATPCharacter::RestartGame() {
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}
// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "FirstGameGameMode.generated.h"

UCLASS(minimalapi)
class AFirstGameGameMode : public AGameMode
{
public:
	GENERATED_BODY()

	AFirstGameGameMode();
	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<APawn> playerRech;
	

	UPROPERTY(EditAnywhere)
		float spawnHeight = 500;
	
	UPROPERTY(EditAnywhere)
		FVector2D spawnMin;
	UPROPERTY(EditAnywhere)
		FVector2D spawnMax;

	UPROPERTY(EditAnywhere)
		FVector2D spawnTimes;


	void SpawnRech();

	void PlayerDeath();

private:
	FTimerHandle timeHandler;

};


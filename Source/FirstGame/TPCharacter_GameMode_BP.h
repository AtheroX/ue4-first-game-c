// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "FirstGameCharacter.h"
#include "GameFramework/Actor.h"

#include "TPCharacter_GameMode_BP.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTGAME_API ATPCharacter_GameMode_BP : public AGameMode
{
	GENERATED_BODY()
		ATPCharacter_GameMode_BP();
		virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<APawn> playerRech;

	UPROPERTY(EditAnywhere)
		float spawnHeight = 500;

	UPROPERTY(EditAnywhere)
		FVector2D spawnMin;
	UPROPERTY(EditAnywhere)
		FVector2D spawnMax;

	void SpawnRech();

};

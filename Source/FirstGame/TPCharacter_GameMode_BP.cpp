// Fill out your copyright notice in the Description page of Project Settings.


#include "TPCharacter_GameMode_BP.h"

ATPCharacter_GameMode_BP::ATPCharacter_GameMode_BP()
{
	PrimaryActorTick.bCanEverTick = true;


}

void ATPCharacter_GameMode_BP::BeginPlay() {
	Super::BeginPlay();

	FTimerHandle timeHandler;
	GetWorldTimerManager().SetTimer(
		timeHandler, this, &ATPCharacter_GameMode_BP::SpawnRech, FMath::RandRange(2, 5), true
	);

}

void ATPCharacter_GameMode_BP::Tick(float deltaTime) {
	Super::Tick(deltaTime);
}

void ATPCharacter_GameMode_BP::SpawnRech() {
	float randX = FMath::RandRange(spawnMin.X, spawnMax.X);
	float randY = FMath::RandRange(spawnMin.Y, spawnMax.Y);

	FVector spawnPos = FVector(randX, randY, spawnHeight);
	FRotator rot = FRotator::ZeroRotator;

	GetWorld()->SpawnActor(playerRech, &spawnPos, &rot);


}
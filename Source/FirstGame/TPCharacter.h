// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Blueprint/UserWidget.h"


#include "TPCharacter.generated.h"

UCLASS()
class FIRSTGAME_API ATPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATPCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		USpringArmComponent* cameraMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		UCameraComponent* followCamera;

	void ControlYawInput(float val);
	void ControlPitchInput(float val);

	void MoveForward(float vel);
	void MoveRight(float vel);
	void Jump();
	void UnJump();

	bool dead;

	UPROPERTY(VisibleAnywhere, BluePrintReadOnly)
		float power;

	UPROPERTY(EditAnywhere)
		float powerTreshold;

	UFUNCTION()
		void OnBeginOverlap(class UPrimitiveComponent* hit, class AActor* other,
			class UPrimitiveComponent* otherComp, int32 otherIndex,
			bool bFromSweep, const FHitResult& hitRes);

	UPROPERTY(EditAnywhere, Category = "HUD")
		TSubclassOf<UUserWidget> powerWidgetClass;

	UUserWidget* powerWidget;

	void RestartGame();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


};
